---
tags: 
  - maven
  - spring-boot-starter-js-css
  - 整合
footer: footer 
---
# 整合到您的spring boot项目
## 前言
- 目前主要是支持通过maven引用。

## 开始
1. 通过maven引入本项目jar包（[查看最新版本](https://search.maven.org/search?q=g:%22top.hmtools%22%20AND%20a:%22spring-boot-starter-js-css%22)）：
```
<dependency>
    <groupId>top.hmtools</groupId>
    <artifactId>spring-boot-starter-js-css</artifactId>
    <version>0.3.0</version>
</dependency>
```
2. 启动您的spring boot 项目。
3. 假设您的spring boot项目监听的端口号是“8080”，在浏览器地址栏输入：“http://localhost:8080/list/js”,如果展示下图界面，说明OK了。
![](/js-files-list.jpg)

## JavaScript
* 假设您的spring boot项目监听的端口号是“8080”

### 获取js文件列表
在浏览器地址栏输入：`http://localhost:8080/list/js`，展现效果：
![](/js-files-list.jpg)

### 获取指定的单个js文件内容
在浏览器地址栏输入：`http://localhost:8080/js/javascript__munge.js`，展现效果：
![](/js_get_one.jpg)

### 获取指定的多个js文件内容
在浏览器地址栏输入：`http://localhost:8080/js/javascript__munge.js,javascript_utf-8Demo.js`，展现效果：
![](/js_get_some.jpg)

### 获取指定字符编码的单个js文件内容
在浏览器地址栏输入：`http://localhost:8080/js/encoding/GBK/javascript_utf-8Demo.js`，展现效果：
![](/js_get_one_gbk.jpg)

### 获取指定字符编码的多个js文件内容
在浏览器地址栏输入：`http://localhost:8080/js/encoding/GBK/javascript__munge.js,javascript_utf-8Demo.js`，展现效果：
![](/js_get_some_gbk.jpg)

### 获取yui压缩后的单个js文件内容
在浏览器地址栏输入：`http://localhost:8080/yui/js/javascript__munge.js`，展现效果：
![](/js_get_one_yui.jpg)

### 获取yui压缩后的多个js文件内容
在浏览器地址栏输入：`http://localhost:8080/yui/js/javascript_utf-8Demo.js,javascript__munge.js`，展现效果：
![](/js_get_some_yui.jpg)

### 获取yui压缩后又指定字符编码的单个js文件内容
在浏览器地址栏输入：`http://localhost:8080/yui/js/javascript__munge.js?munge=true&charset=gbk`，展现效果：
![](/js_get_one_yui_gbk.jpg)

### 获取yui压缩后又指定字符编码的多个js文件内容
在浏览器地址栏输入：`http://localhost:8080/yui/js/javascript__munge.js,javascript_utf-8NoBomDemo.js?munge=true&charset=gbk`，展现效果：
![](/js_get_some_yui_gbk.jpg)

### yui请求参数
- `charset` ： string类型，字符编码，缺省 utf-8
- `linebreak` ：integer类型，指定列插入换行符，-1 为不插入换行符
- `munge` ：Boolean类型，是否进行混淆
- `preserveAllSemiColons` ：Boolean类型，是否保留所有分号“;”
- `disableOptimizations` ：Boolean类型，是否禁用优化



<icp/>