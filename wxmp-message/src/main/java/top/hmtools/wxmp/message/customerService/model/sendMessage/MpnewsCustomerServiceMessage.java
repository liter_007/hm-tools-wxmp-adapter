package top.hmtools.wxmp.message.customerService.model.sendMessage;

import top.hmtools.wxmp.message.customerService.model.BaseSendMessageParam;

/**
 * 消息管理--客服消息--发送图文消息（点击跳转到图文消息页面） 图文消息条数限制在1条以内，注意，如果图文数超过1，则将会返回错误码45008。
 * @author HyboWork
 *
 */
public class MpnewsCustomerServiceMessage extends BaseSendMessageParam{

	private MediaBean mpnews;

	public MediaBean getMpnews() {
		return mpnews;
	}

	public void setMpnews(MediaBean mpnews) {
		this.mpnews = mpnews;
	}

	@Override
	public String toString() {
		return "MpnewsCustomerServiceMessage [mpnews=" + mpnews + ", touser=" + touser + ", msgtype=" + msgtype + "]";
	}
	
	
}
