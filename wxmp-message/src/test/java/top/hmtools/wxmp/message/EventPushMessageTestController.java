package top.hmtools.wxmp.message;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import top.hmtools.wxmp.core.annotation.WxmpController;
import top.hmtools.wxmp.core.annotation.WxmpRequestMapping;
import top.hmtools.wxmp.message.eventPush.model.ClickMenuEventMessage;
import top.hmtools.wxmp.message.eventPush.model.LocationEventMessage;
import top.hmtools.wxmp.message.eventPush.model.QRScanEventMessage;
import top.hmtools.wxmp.message.eventPush.model.QRSubscribeEventMessage;
import top.hmtools.wxmp.message.eventPush.model.SubscribeEventMessage;
import top.hmtools.wxmp.message.eventPush.model.UnsubscribeEventMessage;
import top.hmtools.wxmp.message.eventPush.model.ViewMenuEventMessage;

/**
 * 消息管理 -- 接收事件推送 
 * @author HyboWork
 *
 */
@WxmpController
public class EventPushMessageTestController {

	final Logger logger = LoggerFactory.getLogger(EventPushMessageTestController.class);
	private ObjectMapper objectMapper;
	
	@WxmpRequestMapping
	public void executeMessage(SubscribeEventMessage msg){
		this.printFormatedJson("关注/取消关注事件--订阅", msg);
	}
	
	@WxmpRequestMapping
	public void executeMessage(UnsubscribeEventMessage msg){
		this.printFormatedJson("关注/取消关注事件--取消订阅", msg);
	}
	
	//FIXME TODO 该事件消息 与 SubscribeEventMessage 同 event，区别在 eventKey 
	@WxmpRequestMapping
	public void executeMessage(QRSubscribeEventMessage msg){
		this.printFormatedJson("扫描带参数二维码事件--1. 用户未关注时，进行关注后的事件推送", msg);
	}
	
	@WxmpRequestMapping
	public void executeMessage(QRScanEventMessage msg){
		this.printFormatedJson("扫描带参数二维码事件--2. 用户已关注时的事件推送", msg);
	}
	
	@WxmpRequestMapping
	public void executeMessage(LocationEventMessage msg){
		this.printFormatedJson("上报地理位置事件", msg);
	}
	
	@WxmpRequestMapping
	public void executeMessage(ClickMenuEventMessage msg){
		this.printFormatedJson("自定义菜单事件--点击菜单拉取消息时的事件推送", msg);
	}
	
	
	@WxmpRequestMapping
	public void executeMessage(ViewMenuEventMessage msg){
		this.printFormatedJson("自定义菜单事件--点击菜单跳转链接时的事件推送", msg);
	}
	
	
	
	
	
	
	
	
	/**
	 * 格式化打印json字符串到控制台
	 * @param title
	 * @param obj
	 */
	protected synchronized void printFormatedJson(String title,Object obj) {
		if(this.objectMapper == null){
			this.objectMapper = new ObjectMapper();
		}
		try {
			//阿里的fastjson具有很好的兼容性，所以才多次一举
			String jsonString = JSON.toJSONString(obj);
			Object tempObj = JSON.parse(jsonString);
			String formatedJsonStr = this.objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(tempObj);
			this.logger.info("\n{}：\n{}",title,formatedJsonStr);
		} catch (JsonProcessingException e) {
			this.logger.error("格式化打印json异常：",e);
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
